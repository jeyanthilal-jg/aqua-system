jQuery(document).ready(function($) { 
  // place your code here
  $("#species-form").validate({
		rules: {
			feedname: {
				required: true
			},
			speciestype:{
				required: true
			},
			maxweight:{
				required: true,
				number: true
			}
		},
		messages: {
			feedname: {
				required: "Please Enter the Feed Name"
			},
			speciestype: {
				required: "Please Enter Species Name"
			},
			maxweight: {
				required: "Please Enter the max weight",
				number : "please Enter value in Number"
			}
		}
	});
	
  $("#feed-form").validate({
		rules: {
			feedname: {
				required: true
			},
			feed_type:{
				required: true
			},
			size:{
				required: true,
				number: true
			}
		},
		messages: {
			
			feedname: {
				required: "Please Enter the Feed Name"
			},
			feed_type: {
				required: "Please Enter Feed Type"
			},
			size: {
				required: "Please Enter the Size",
				number : "please Enter value in Number"
			}
		}
	});
	
	
});