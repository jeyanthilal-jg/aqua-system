<?php

function fb_pr($pdata)
{
	echo "<pre>";
	print_r($pdata);
	echo "</pre>";
}

function fb_generate_pagination($params = array()){
	$ci =& get_instance();
	$ci->load->library('pagination');

	
	/* This Application Must Be Used With BootStrap 3 *  */
	$config['full_tag_open'] = "<ul class='pagination'>";
	$config['full_tag_close'] ="</ul>";
	$config['num_tag_open'] = '<li class="paginate_button page-item">';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = "<li class='paginate_button page-item active'><a href='#'>";
	$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	$config['next_tag_open'] = "<li class=\"paginate_button page-item next\">";
	$config['next_tag_close'] = "</li>";
	$config['prev_tag_open'] = "<li class=\"paginate_button page-item previous\">";
	$config['prev_tag_close'] = "</li>";
	$config['first_tag_open'] = "<li class=\"paginate_button page-item\">";
	$config['first_tag_close'] = "</li>";
	$config['last_tag_open'] = "<li class=\"paginate_button page-item\">";
	$config['last_tag_close'] = "</li>";
	
	//$config['base_url'] = site_url('/home/index');
	$config['base_url'] = $params['page_burl'];
	$config['total_rows'] = $params['total_rows'];
	$config['per_page'] = $params['per_page'];
	$config['use_page_numbers'] = TRUE;
	$config['reuse_query_string'] = TRUE;
	$config['uri_segment'] = $params['uri_segment'];

	$ci->pagination->initialize($config);

	$plinks = $ci->pagination->create_links();
	return $plinks;
}

function fb_combo_arr($table_name = "ponds1"){
	$ci =& get_instance();
	$msg = $ci->fb_rest->combo_list($table_name);
	$cmb_list = array();
	if($msg["status"] == "success"){
		$cmb_list = $msg["combo_list"];
	}
	return $cmb_list;
}