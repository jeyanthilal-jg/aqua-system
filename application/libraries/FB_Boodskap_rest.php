<?php
/**
 * Name:    Fourbends Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to connect our PHP system to Boodskap API.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class FB_Boodskap_rest
{

	protected $CI;
    protected $tables;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->config->load('fb_boodskap', TRUE);
		$this->tables = $this->CI->config->item('tables', 'fb_boodskap');
		require_once( APPPATH . 'third_party/boodskap/autoload.php');
	}
	
	public function login($email, $password){
		
		$api_instance = new Swagger\Client\Api\LoginApi();
		
		try {
			$result = $api_instance->login($email, $password);
			
			$user_token = $result->getToken();
			$domain_key = $result->getDomainKey();
			$api_key = $result->getApiKey();
			$cUser = $result->getUser();
			$email = $cUser->getEmail();
			$first_name = $cUser->getFirstName();
			$last_name = $cUser->getLastName();
			$country = $cUser->getCountry();
			$state = $cUser->getState();
			$city = $cUser->getCity();
			$address = $cUser->getAddress();
			$zipcode = $cUser->getZipcode();
			$locale = $cUser->getLocale();
			$timezone = $cUser->getTimezone();
			
			$udata = array(
				"user_token" => $user_token,
				"domain_key" => $domain_key,
				"api_key" => $api_key,
			    "email" => $email,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"country" => $country,
				"state" => $state,
				"city" => $city,
				"address" => $address,
				"zipcode" => $zipcode,
				"locale" => $locale,
				"timezone" => $timezone
			);
			$this->CI->session->set_userdata($udata); // Set the session data
			$msg = array("status" => "success", "message" => "Successfully Loggedin");
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling LoginApi->login: ".$smsg);
			$msg = array("status" => "fail", "message" => "Invalid Login");
			return $msg;
		}
	}
	
	public function isloggedin(){
		$status = $this->CI->session->has_userdata("user_token");
		return $status;
	}
	
	public function get_fbuser_data($name){
		$udata = $this->CI->session->has_userdata($name);
		if($udata){
			return $this->CI->session->userdata($name);
		}else{
			return "";
		}
	}
	
	public function create_record($table_name = "", $idata = array()){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$tcond = (isset($this->tables[$table_name]) && !empty($idata) );
		if($tcond){
			$api_instance = new Swagger\Client\Api\InsertUpdateRecordApi();
			$tbl_id = $this->tables[$table_name]; // table idata
			$user_token = $this->get_fbuser_data("user_token");
			$ndata = $this->fb_setrecord($table_name, $idata);
			$sdata = json_encode($ndata);
			
			try {
				$result = $api_instance->upsertRecord($user_token, $tbl_id, $sdata);
				fb_pr($result);
				$msg = array("status" => "success", "message" => "Successfully created record");
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling InsertUpdateRecordApi->upsertRecord: ".$smsg);
				$msg = array("status" => "fail", "message" => "Invalid Table or Data or Boodskap Server issue");
				return $msg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table or Data");
			return $msg;
		}
		
	}
	
	public function test_list_record($table_name = "ponds1"){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			fb_pr($msg);
			return $msg;
		}
		
		$api_instance = new Swagger\Client\Api\SearchApi();
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
		$queryObj = array();
		//$queryObj['query']= new stdClass();
		//"match_all": {} 
		$queryObj['size'] = 10000;
		$qmain = new stdClass();
		$qmain->match_all = new stdClass();
		$queryObj['query']= $qmain;
		$queryObj['sort'] = array("updatedtime" => array("order" => "desc" )); 
		$qstr = json_encode($queryObj);
		fb_pr($qstr);
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$sresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$result = $sresult->getResult();
			fb_pr($result);
		} catch (Exception $e) {
			echo 'Exception when calling SearchApi->search: ', $e->getMessage(), PHP_EOL;
		}

	}
	
	// Parameters list ("page_no", "per_page", "uri_segment", "search", "sort_fld", "sort_dir", "page_burl", "table_name")
	
	public function list_record($params = array()){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$pre_params =  array("page_no" => "1", "per_page" => "10", "uri_segment" => "2", "search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc",
		"page_burl" => site_url("/"), "table_name" => "");
		
		foreach($pre_params as $pk => $pv){
			if(!isset($params[$pk]) || empty($params[$pk])){
				$params[$pk] = $pv;
			}
		}

		extract($params);
		
		$from = ($page_no <= 1) ? "0" : ( $page_no - 1 ) ;
		$from = ($from * $per_page);
		//"from" : 0
		$page_params = compact("per_page", "uri_segment", "page_burl");
		
		$tcond = isset($this->tables[$table_name]);
		if($tcond){
			$api_instance = new Swagger\Client\Api\SearchApi();
			$atoken = $this->get_fbuser_data("user_token");
			$tbl_id = $this->tables[$table_name];
			$type = "RECORD";
			$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
			$queryObj = array();
			$queryObj['query']= new stdClass();
			$queryObj['size'] = $per_page;
			$queryObj['from'] = $from;
			$queryObj['sort'] = array($sort_fld => array("order" => $sort_dir )); 
			$qstr = json_encode($queryObj);
			$query['query']= $qstr;
			$query['method']="GET";
			$repositary = "";
			$mapping = "";
			try {
				$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
				$sresult = $oresult->getResult();
				$aresult = json_decode($sresult, true);
				$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
				$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
				$page_params["total_rows"] = $total_count;
				$page_links = fb_generate_pagination($page_params);
				$page_links = ( !empty($result_set) ) ? $page_links : "";
				$msg = array("status" => "success", "message" => "Search result got it",
				"page_links" => $page_links, "result_set" => $result_set);
				return $msg;
			} catch (Exception $e) {
				$smsg = $e->getMessage();
				log_message('error', "Exception when calling SearchApi->search: ".$smsg);
				$msg = array("status" => "fail", "message" => "Search functionality error");
				return $msg;
			}
			
		}else{
			$msg = array("status" => "fail", "message" => "Invalid Table name");
			return $msg;
		}
		
		
	}
	
	private function fb_setrecord($table_name = "", $idata = array()){
		$newdata = array();
		$tblkeys = $this->CI->config->item($table_name, 'fb_boodskap');
		foreach($tblkeys as $tblkey){
			$newdata[$tblkey] = isset($idata[$tblkey]) ? $idata[$tblkey] : "";
		}
		return $newdata;
	}
	
	public function combo_list($table_name = "ponds1"){
		if(!$this->isloggedin()){
			$msg = array("status" => "fail", "message" => "Please login to continue");
			return $msg;
		}
		
		$combo_list = $this->CI->config->item('combo_list', 'fb_boodskap');
		if(!isset($combo_list[$table_name])){
			$msg = array("status" => "fail", "message" => "Please give the correct table name");
			return $msg;
		}
		
		$cmb_cfg = $combo_list[$table_name];
		$kfld_val = $cmb_cfg["key"];
		$vfld_val = $cmb_cfg["value"];
		$sfld_val = $cmb_cfg["sort_fld"];
		$sdir_val = $cmb_cfg["sort_dir"];
		$acombo = array();
		
		$api_instance = new Swagger\Client\Api\SearchApi();
		$atoken = $this->get_fbuser_data("user_token");
		$tbl_id = $this->tables[$table_name];
		$type = "RECORD";
		$query = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | SearchQuery JSON
		$queryObj = array();
		$queryObj['size'] = 10000;
		$qmain = new stdClass();
		$qmain->match_all = new stdClass();
		$queryObj['query']= $qmain;
		$queryObj['sort'] = array($sfld_val => array("order" => $sdir_val )); 
		$qstr = json_encode($queryObj);
		
		$query['query']= $qstr;
		$query['method']="GET";
		$repositary = "";
		$mapping = "";
		try {
			$oresult = $api_instance->search($atoken, $type, $query, $tbl_id, $repositary, $mapping);
			$sresult = $oresult->getResult();
			$aresult = json_decode($sresult, true);
			$total_count = isset($aresult["hits"]["total"]) ? $aresult["hits"]["total"]: "0";
			$result_set = isset($aresult["hits"]["hits"]) ? $aresult["hits"]["hits"] : array();
			foreach($result_set as $row){
				$src = $row["_source"];
				$k = $src[$kfld_val];
				$v = $src[$vfld_val];
				$acombo[$k] = $v;
			}
			$msg = array("status" => "success", "message" => "Fetch Table: $table_name result successfully", "combo_list" => $acombo);
			return $msg;
		} catch (Exception $e) {
			$smsg = $e->getMessage();
			log_message('error', "Exception when calling SearchApi->search: ".$smsg);
			$msg = array("status" => "fail", "message" => "Search functionality error");
			return $msg;
		}
	}
	
	
}
