<!-- Left Panel -->

<aside id="left-panel" class="left-panel">
  <nav class="navbar navbar-expand-sm navbar-default">
    <div class="navbar-header">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation"> <i class="fa fa-bars"></i> </button>
      <a class="navbar-brand" href="./"><img src="<?php echo base_url();?>assets/images/logo.png" alt="Logo"></a> <a class="navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/logo2.png" alt="Logo"></a> </div>
    <div id="main-menu" class="main-menu collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"> <a href="<?php echo site_url("/dashboard");?>"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a> </li>
        <h3 class="menu-title">Menus</h3>
        <!-- /.menu-title -->
        
        <li class="menu-item-has-children dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-th"></i>Ponds</a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("/pondlist");?>">Pond List</a></li>
            <li><i class="fa fa-list"></i><a href="<?php echo site_url("/pondcleaning");?>">Pond Cleaning</a></li>
          </ul>
        </li>
        <li> <a href="<?php echo site_url("/species");?>"><i class="menu-icon fa fa-odnoklassniki"></i>Species </a> </li>
        <li> <a href="<?php echo site_url("/feed");?>"><i class="menu-icon fa fa-pagelines"></i>Feed </a> </li>
        <li class="menu-item-has-children dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-signal"></i>Stock</a>
          <ul class="sub-menu children dropdown-menu">
            <li><i class="fa fa-list"></i><a href="currentstock.html">Current Stock</a></li>
            <li><i class="fa fa-list"></i><a href="fishstock.html">Fish Stock</a></li>
          </ul>
        </li>
        <li> <a href="<?php echo site_url("/distribution");?>"><i class="menu-icon fa fa-clone"></i>Distribution </a> </li>
        <li> <a href="<?php echo site_url("/harvest");?>"><i class="menu-icon fa fa-archive"></i>Harvest </a> </li>
        <li> <a href="<?php echo site_url("/mortality");?>"><i class="menu-icon fa fa-laptop"></i>Mortality </a> </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </nav>
</aside>
<!-- /#left-panel --> 

<!-- Left Panel --> 
