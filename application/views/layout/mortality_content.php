<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>


  <div class="breadcrumbs">
    <div class="col-sm-4">
      <div class="page-header float-left">
        <div class="page-title">
          <h1>Mortality</h1>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="page-header float-right">
        <div class="page-title">
          <ol class="breadcrumb text-right">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Mortality</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  
  <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title">Mortality</strong> </div>
            <div class="card-body">
              <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Pond name</th>
                    <th>Height</th>
                    <th>Weight</th>
                    <th>Depth</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Sample name</td>
                    <td>height</td>
                    <td>weight</td>
                    <td>depth</td>
                    <td>date</td>
                    <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
                  </tr>
                  <tr>
                    <td>Sample name</td>
                    <td>height</td>
                    <td>weight</td>
                    <td>depth</td>
                    <td>date</td>
                    <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
                  </tr>
                  <tr>
                    <td>Sample name</td>
                    <td>height</td>
                    <td>weight</td>
                    <td>depth</td>
                    <td>date</td>
                    <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
                  </tr>
                  <tr>
                    <td>Sample name</td>
                    <td>height</td>
                    <td>weight</td>
                    <td>depth</td>
                    <td>date</td>
                    <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
                  </tr>
                  <tr>
                    <td>Sample name</td>
                    <td>height</td>
                    <td>weight</td>
                    <td>depth</td>
                    <td>date</td>
                    <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
                  </tr>
                  <tr>
                    <td>Sample name</td>
                    <td>height</td>
                    <td>weight</td>
                    <td>depth</td>
                    <td>date</td>
                    <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
                  </tr>
                  <tr>
                    <td>Sample name</td>
                    <td>height</td>
                    <td>weight</td>
                    <td>depth</td>
                    <td>date</td>
                    <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title">Add Mortality</strong> </div>
            <div class="card-body">
              <form>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="pondname">Pond name</label>
                    <input type="text" class="form-control" id="pondname" placeholder="Pond name">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="speciestype">Species type</label>
                    <select id="speciestype" class="form-control">
                      <option selected>Choose...</option>
                      <option>...</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="weight">Weight</label>
                    <input type="text" class="form-control" id="weight" placeholder="Weight">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="count">Count</label>
                    <input type="text" class="form-control" id="count" placeholder="Count">
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputCity">Date</label>
                    <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker4"/>
                      <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Sign in</button>
                <button type="submit" class="btn btn-secondary">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
  



</div><!-- /#right-panel -->

<!-- Right Panel -->
