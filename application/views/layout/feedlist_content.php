<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>

<div class="breadcrumbs">
<div class="col-sm-4">
  <div class="page-header float-left">
    <div class="page-title">
      <h1>Feed</h1>
    </div>
  </div>
</div>
<div class="col-sm-8">
  <div class="page-header float-right">
    <div class="page-title">
      <ol class="breadcrumb text-right">
        <li><a href="#">Dashboard</a></li>
        <li class="active">Feed List</li>
      </ol>
    </div>
  </div>
</div>
</div>

<div class="content mt-3">
<div class="animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title">Feed List</strong> </div>
        <div class="card-body">
		<div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
		<div class="row">
					  <div class="col-sm-12 col-md-6">
						 <div class="dataTables_length" id="bootstrap-data-table_length">
							<label>
							   Show 
							   <select name="per_page" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
								  <option value="10">10</option>
								  <option value="20">20</option>
								  <option value="50">50</option>
								  <option value="-1">All</option>
							   </select>
							   entries
							</label>
						 </div>
					  </div>
					  <div class="col-sm-12 col-md-6">
						 <div id="bootstrap-data-table_filter" class="dataTables_filter float-right"><label>Search:<input type="search" name="search" class="form-control form-control-sm" placeholder="" ></label></div>
						 <input type="hidden" name="sort_fld" value="">
						 <input type="hidden" name="sort_dir" value="">
					  </div>
				   </div>
          <table id="bootstrap-data-table" class="table table-striped table-bordered" role="grid">
            <thead>
              <tr>
                <th>Feed Name</th>
				<th>Feed Type</th>
				<th>Size</th>
				<th>Action</th>
              </tr>
            </thead>
            <tbody>
						   
						   <?php foreach($result_set as $row): 
							  $source = $row["_source"];
						   ?>
						     <tr role="row">
							  <td class=""><?php echo $source["feedname"]; ?></td>
							  <td><?php echo $source["feed_type"]; ?></td>
							  <td><?php echo $source["size"]; ?></td>
							  
							  <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
						    </tr>
						   <?php endforeach; ?>
						</tbody>
          </table>
		     <div class="row">
				  <div class="col-sm-12 col-md-5">
					 <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>-->
				  </div>
				  <div class="col-sm-12 col-md-7">
					 <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
					    <?php echo $page_links; ?>
						<!--<ul class="pagination">
						   <li class="paginate_button page-item previous" id="bootstrap-data-table_previous"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
						   <li class="paginate_button page-item active"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
						   <li class="paginate_button page-item"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
						   <li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
						   <li class="paginate_button page-item next" id="bootstrap-data-table_next"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="4" tabindex="0" class="page-link">Next</a></li>
						</ul> -->
					 </div>
				  </div>
			   </div>
		  </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"> <strong class="card-title">Add Feed</strong> </div>
        <div class="card-body">
          <form name="feed" id="feed-form" action="<?php echo base_url('feed/add_record');?>" method="post">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="feedname">Feed name</label>
                    <input type="text" class="form-control" name="feedname" placeholder="Feed name">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="feedtype">Feed type</label>
                    <select id="feedtype" class="form-control" name="feed_type">
                      <option selected hidden disabled>Choose Feed Type</option>
                      <option value="natural">natural</option>
					  <option value="chemical">chemical</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="size">Size</label>
					<input type="text" class="form-control" name="size" placeholder="Feed Size">
                  <!--  <select id="size" class="form-control" name="size">
                      <option selected hidden disabled>Choose Feed Size</option>
                      <option value="0.5">0.5mm</option>
					  <option value="1.0">1.0mm</option>
					  <option value="1.5">1.5mm</option>
					  <option value="2.0">2.0mm</option>
					  <option value="2.5">2.5mm</option>
					  <option value="3.0">3.0mm</option>
					  <option value="3.5">3.5mm</option>
					  <option value="4.0">4.0mm</option>
					  <option value="4.5">4.5mm</option>
					  <option value="5.0">5.0mm</option>
					  <option value="5.5">5.5mm</option>
					  <option value="6.0">6.0mm</option>
					  <option value="6.5">6.5mm</option>
					  <option value="7.0">7.0mm</option>
					  <option value="7.5">7.5mm</option>
                    </select>-->
                  </div>
                
				</div>
            <button type="submit" class="btn btn-primary" >Save</button>
            <button type="reset" class="btn btn-secondary">Cancel</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- .animated --> 
</div>

</div><!-- /#right-panel -->

<!-- Right Panel -->