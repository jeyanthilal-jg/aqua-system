<div id="right-panel" class="right-panel">

<?php $this->load->view('./include/top_menu'); ?>
  
  
  <div class="breadcrumbs">
    <div class="col-sm-4">
      <div class="page-header float-left">
        <div class="page-title">
          <h1>Species</h1>
        </div>
      </div>
    </div>
    <div class="col-sm-8">
      <div class="page-header float-right">
        <div class="page-title">
          <ol class="breadcrumb text-right">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Species</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content mt-3">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title">Species</strong> </div>
            <div class="card-body">
             <div id="bootstrap-data-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
			<div class="row">
						  <div class="col-sm-12 col-md-6">
							 <div class="dataTables_length" id="bootstrap-data-table_length">
								<label>
								   Show 
								   <select name="per_page" aria-controls="bootstrap-data-table" class="form-control form-control-sm">
									  <option value="10">10</option>
									  <option value="20">20</option>
									  <option value="50">50</option>
									  <option value="-1">All</option>
								   </select>
								   entries
								</label>
							 </div>
						  </div>
						  <div class="col-sm-12 col-md-6">
							 <div id="bootstrap-data-table_filter" class="dataTables_filter float-right"><label>Search:<input type="search" name="search" class="form-control form-control-sm" placeholder="" ></label></div>
							 <input type="hidden" name="sort_fld" value="">
							 <input type="hidden" name="sort_dir" value="">
						  </div>
					   </div>
			  <table id="bootstrap-data-table" class="table table-striped table-bordered" role="grid">
				<thead>
				  <tr>
					<th>Species Name</th>
					<th>Feed Name</th>
					<th>Maximum Weight</th>
					<th>Action</th>
				  </tr>
				</thead>
				<tbody>
							   
							   <?php foreach($result_set as $row): 
								  $source = $row["_source"];
							   ?>
								 <tr role="row">
								  <td class=""><?php echo $source["species_type"]; ?></td>
								  <td><?php echo $source["feedname"]; ?></td>
								  <td><?php echo $source["max_weight"]; ?></td>
								  
								  <td><a href="#"><i class="fa fa-eye"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp; <a href="#"><i class="fa fa-trash"></i></a></td>
								</tr>
							   <?php endforeach; ?>
							</tbody>
			  </table>
		     <div class="row">
				  <div class="col-sm-12 col-md-5">
					 <!--<div class="dataTables_info" id="bootstrap-data-table_info" role="status" aria-live="polite">Showing 11 to 20 of 25 entries</div>-->
				  </div>
				  <div class="col-sm-12 col-md-7">
					 <div class="dataTables_paginate paging_simple_numbers" id="bootstrap-data-table_paginate">
					    <?php echo $page_links; ?>
						<!--<ul class="pagination">
						   <li class="paginate_button page-item previous" id="bootstrap-data-table_previous"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
						   <li class="paginate_button page-item active"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
						   <li class="paginate_button page-item"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
						   <li class="paginate_button page-item "><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
						   <li class="paginate_button page-item next" id="bootstrap-data-table_next"><a href="#" aria-controls="bootstrap-data-table" data-dt-idx="4" tabindex="0" class="page-link">Next</a></li>
						</ul> -->
					 </div>
				  </div>
			   </div>
		  </div>
        </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header"> <strong class="card-title">Add Species</strong> </div>
            <div class="card-body">
              <form name="species" id="species-form" method="post" action="<?php echo base_url('species/add_record');?>">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="speciestype">Species type</label>
					<input type="text" name="speciestype" class="form-control" placeholder="Species Name"/>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="feedname">Feed name</label>
                    <select id="feedname" class="form-control" name="feedname">
                      <option selected hidden disabled>Choose...</option>
                      <option>sample1</option>
					  <option>sample2</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="maxweight">Max weight</label>
                    <input type="text" name="maxweight" class="form-control" placeholder="Maximum Weight"/>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .animated --> 
  </div>
  
  
  
</div><!-- /#right-panel -->

<!-- aaRight Panel -->
