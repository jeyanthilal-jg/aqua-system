<?php
/**
 * Name:    Fourbends - Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to refer the tables list in boodskap.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$config['tables'] = array( "ponds" => "1000", "species" => "1001",
                        "fish_stock" => "1002", "distribution" => "1003",
						"harvest" => "1004", "mortality" => "1005",
                        "current_stock" => "1006", "pond_cleaning" => "1007",
                        "feeds" => "1008", "ponds1" => "2000"
                    );

$config["ponds1"] = array("pondname", "depth", "height", "width", "built_date", "createdtime", "updatedtime"); // Testing ponds
					
$config["ponds"] = array("pondname", "depth", "height", "width", "built_date", "createdtime", "updatedtime");

$config["species"] = array("feedname", "max_weight", "species_type", "createdtime", "updatedtime");

$config["fish_stock"] = array("pondname", "species_type", "count", "fertilizer", "water_type", "weight", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["distribution"] = array("from_pond", "to_pond", "species_type", "count", "updatedby", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["harvest"] = array("pondname", "species_type", "count", "weight", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["mortality"] = array("pondname", "species_type", "reason", "count", "weight", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["current_stock"] = array("pondname", "species_type", "count", "weight", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["pond_cleaning"] = array("pondname", "cleanedby", "_day", "_month", "_year", "createdtime", "updatedtime");

$config["feeds"] = array("feedname", "size", "feed_type", "createdtime", "updatedtime");

$config['combo_list'] = array(
   "ponds1" => array( "key" => "pondname", "value" => "pondname", "sort_fld" => "pondname", "sort_dir" => "asc" ), 
   "ponds" => array( "key" => "pondname", "value" => "pondname", "sort_fld" => "pondname", "sort_dir" => "asc" ), 
   "feeds" => array( "key" => "feedname", "value" => "feedname", "sort_fld" => "feedname", "sort_dir" => "asc" ),
   "species" => array( "key" => "species_type", "value" => "species_type", "sort_fld" => "species_type", "sort_dir" => "asc" )
);

