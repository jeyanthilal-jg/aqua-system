<?php
/**
 * PropertyCommand
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swaagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Domain API
 *
 * Boodskap IoT Platform (Domain API)
 *
 * OpenAPI spec version: 1.0.6
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;

/**
 * PropertyCommand Class Doc Comment
 *
 * @category    Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PropertyCommand implements ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'PropertyCommand';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = [
        'target' => 'string',
        'asset_id' => 'string',
        'device_id' => 'string',
        'device_model_id' => 'string',
        'user_id' => 'string',
        'geofence_id' => 'string',
        'group_id' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerFormats = [
        'target' => null,
        'asset_id' => null,
        'device_id' => null,
        'device_model_id' => null,
        'user_id' => null,
        'geofence_id' => null,
        'group_id' => 'int32'
    ];

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = [
        'target' => 'target',
        'asset_id' => 'assetId',
        'device_id' => 'deviceId',
        'device_model_id' => 'deviceModelId',
        'user_id' => 'userId',
        'geofence_id' => 'geofenceId',
        'group_id' => 'groupId'
    ];


    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = [
        'target' => 'setTarget',
        'asset_id' => 'setAssetId',
        'device_id' => 'setDeviceId',
        'device_model_id' => 'setDeviceModelId',
        'user_id' => 'setUserId',
        'geofence_id' => 'setGeofenceId',
        'group_id' => 'setGroupId'
    ];


    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = [
        'target' => 'getTarget',
        'asset_id' => 'getAssetId',
        'device_id' => 'getDeviceId',
        'device_model_id' => 'getDeviceModelId',
        'user_id' => 'getUserId',
        'geofence_id' => 'getGeofenceId',
        'group_id' => 'getGroupId'
    ];

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    public static function setters()
    {
        return self::$setters;
    }

    public static function getters()
    {
        return self::$getters;
    }

    const TARGET_ASSET = 'ASSET';
    const TARGET_ASSET_GROUP = 'ASSET_GROUP';
    const TARGET_DEVICE = 'DEVICE';
    const TARGET_DEVICE_GROUP = 'DEVICE_GROUP';
    const TARGET_DEVICE_MODEL = 'DEVICE_MODEL';
    const TARGET_DOMAIN = 'DOMAIN';
    const TARGET_DOMAIN_ASSET_GROUP = 'DOMAIN_ASSET_GROUP';
    const TARGET_DOMAIN_DEVICE_GROUP = 'DOMAIN_DEVICE_GROUP';
    const TARGET_DOMAIN_USER_GROUP = 'DOMAIN_USER_GROUP';
    const TARGET_GEOFENCE = 'GEOFENCE';
    const TARGET_USER = 'USER';
    const TARGET_USER_GROUP = 'USER_GROUP';
    

    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public function getTargetAllowableValues()
    {
        return [
            self::TARGET_ASSET,
            self::TARGET_ASSET_GROUP,
            self::TARGET_DEVICE,
            self::TARGET_DEVICE_GROUP,
            self::TARGET_DEVICE_MODEL,
            self::TARGET_DOMAIN,
            self::TARGET_DOMAIN_ASSET_GROUP,
            self::TARGET_DOMAIN_DEVICE_GROUP,
            self::TARGET_DOMAIN_USER_GROUP,
            self::TARGET_GEOFENCE,
            self::TARGET_USER,
            self::TARGET_USER_GROUP,
        ];
    }
    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['target'] = isset($data['target']) ? $data['target'] : null;
        $this->container['asset_id'] = isset($data['asset_id']) ? $data['asset_id'] : null;
        $this->container['device_id'] = isset($data['device_id']) ? $data['device_id'] : null;
        $this->container['device_model_id'] = isset($data['device_model_id']) ? $data['device_model_id'] : null;
        $this->container['user_id'] = isset($data['user_id']) ? $data['user_id'] : null;
        $this->container['geofence_id'] = isset($data['geofence_id']) ? $data['geofence_id'] : null;
        $this->container['group_id'] = isset($data['group_id']) ? $data['group_id'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = [];

        if ($this->container['target'] === null) {
            $invalid_properties[] = "'target' can't be null";
        }
        $allowed_values = $this->getTargetAllowableValues();
        if (!in_array($this->container['target'], $allowed_values)) {
            $invalid_properties[] = sprintf(
                "invalid value for 'target', must be one of '%s'",
                implode("', '", $allowed_values)
            );
        }

        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['target'] === null) {
            return false;
        }
        $allowed_values = $this->getTargetAllowableValues();
        if (!in_array($this->container['target'], $allowed_values)) {
            return false;
        }
        return true;
    }


    /**
     * Gets target
     * @return string
     */
    public function getTarget()
    {
        return $this->container['target'];
    }

    /**
     * Sets target
     * @param string $target
     * @return $this
     */
    public function setTarget($target)
    {
        $allowed_values = $this->getTargetAllowableValues();
        if (!in_array($target, $allowed_values)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'target', must be one of '%s'",
                    implode("', '", $allowed_values)
                )
            );
        }
        $this->container['target'] = $target;

        return $this;
    }

    /**
     * Gets asset_id
     * @return string
     */
    public function getAssetId()
    {
        return $this->container['asset_id'];
    }

    /**
     * Sets asset_id
     * @param string $asset_id
     * @return $this
     */
    public function setAssetId($asset_id)
    {
        $this->container['asset_id'] = $asset_id;

        return $this;
    }

    /**
     * Gets device_id
     * @return string
     */
    public function getDeviceId()
    {
        return $this->container['device_id'];
    }

    /**
     * Sets device_id
     * @param string $device_id
     * @return $this
     */
    public function setDeviceId($device_id)
    {
        $this->container['device_id'] = $device_id;

        return $this;
    }

    /**
     * Gets device_model_id
     * @return string
     */
    public function getDeviceModelId()
    {
        return $this->container['device_model_id'];
    }

    /**
     * Sets device_model_id
     * @param string $device_model_id
     * @return $this
     */
    public function setDeviceModelId($device_model_id)
    {
        $this->container['device_model_id'] = $device_model_id;

        return $this;
    }

    /**
     * Gets user_id
     * @return string
     */
    public function getUserId()
    {
        return $this->container['user_id'];
    }

    /**
     * Sets user_id
     * @param string $user_id
     * @return $this
     */
    public function setUserId($user_id)
    {
        $this->container['user_id'] = $user_id;

        return $this;
    }

    /**
     * Gets geofence_id
     * @return string
     */
    public function getGeofenceId()
    {
        return $this->container['geofence_id'];
    }

    /**
     * Sets geofence_id
     * @param string $geofence_id
     * @return $this
     */
    public function setGeofenceId($geofence_id)
    {
        $this->container['geofence_id'] = $geofence_id;

        return $this;
    }

    /**
     * Gets group_id
     * @return int
     */
    public function getGroupId()
    {
        return $this->container['group_id'];
    }

    /**
     * Sets group_id
     * @param int $group_id
     * @return $this
     */
    public function setGroupId($group_id)
    {
        $this->container['group_id'] = $group_id;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Swagger\Client\ObjectSerializer::sanitizeForSerialization($this));
    }
}


