# Swagger\Client\LoginApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**login**](LoginApi.md#login) | **GET** /domain/login/{email}/{passwd} | Login


# **login**
> \Swagger\Client\Model\UserDomain login($email, $passwd)

Login

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\LoginApi();
$email = "email_example"; // string | 
$passwd = "passwd_example"; // string | 

try {
    $result = $api_instance->login($email, $passwd);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LoginApi->login: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **string**|  |
 **passwd** | **string**|  |

### Return type

[**\Swagger\Client\Model\UserDomain**](../Model/UserDomain.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

