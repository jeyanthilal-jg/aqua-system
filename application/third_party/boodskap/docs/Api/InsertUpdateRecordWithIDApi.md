# Swagger\Client\InsertUpdateRecordWithIDApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**upsertRecordWithID**](InsertUpdateRecordWithIDApi.md#upsertRecordWithID) | **POST** /reocrd/insert/static/{atoken}/{rid}/{rkey} | Insert/Update Record With ID


# **upsertRecordWithID**
> \Swagger\Client\Model\InsertResult upsertRecordWithID($atoken, $rid, $rkey, $data)

Insert/Update Record With ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\InsertUpdateRecordWithIDApi();
$atoken = "atoken_example"; // string | Auth token of the logged in user
$rid = 56; // int | Well defined unique record ID
$rkey = "rkey_example"; // string | Record's unique identifier
$data = "data_example"; // string | Stringified record JSON object

try {
    $result = $api_instance->upsertRecordWithID($atoken, $rid, $rkey, $data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InsertUpdateRecordWithIDApi->upsertRecordWithID: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **rid** | **int**| Well defined unique record ID |
 **rkey** | **string**| Record&#39;s unique identifier |
 **data** | **string**| Stringified record JSON object |

### Return type

[**\Swagger\Client\Model\InsertResult**](../Model/InsertResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

