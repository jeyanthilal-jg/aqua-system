# Lookup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target** | **string** |  | 
**data_type** | **string** |  | 
**name** | **string** |  | 
**value** | **string** |  | 
**asset_id** | **string** |  | [optional] 
**device_id** | **string** |  | [optional] 
**device_model_id** | **string** |  | [optional] 
**user_id** | **string** |  | [optional] 
**geofence_id** | **string** |  | [optional] 
**group_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


