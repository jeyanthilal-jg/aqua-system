# Location

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_id** | **string** |  | 
**at** | **string** |  | [optional] 
**lat** | **double** |  | [optional] 
**lon** | **double** |  | [optional] 
**device_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


