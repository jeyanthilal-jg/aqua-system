# MLModelInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**records** | **int** |  | 
**relation** | **string** |  | 
**classifier** | **string** |  | 
**prediction_type** | **string** |  | 
**train_every** | **int** |  | [optional] 
**created_at** | **int** |  | [optional] 
**trained_at** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


