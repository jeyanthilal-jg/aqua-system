# SearchResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**http_code** | **int** |  | 
**result** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


