<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Species extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
  public function __construct(){
  parent::__construct();
  $this->load->helper('date');
 }

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("per_page", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/species");
			$this->table="species";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			$msg  = $this->fb_rest->list_record($params);
		
		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/species_content", $data);
			}else{
				//$this->load->view("error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
	}
	public function add_record(){
		$form_data = $this->input->post();
		$speciestype = $this->input->post("speciestype");
		$maxweight = $this->input->post("maxweight");
		$feedname = $this->input->post("feedname");
		$table_name = "species";
		$idata = array("feedname" => $feedname, 
		"max_weight" => $maxweight, 
		"species_type" => $speciestype,
		"createdtime" => now(), 
		"updatedtime" => now());
		
		$result = $this->fb_rest->create_record($table_name, $idata);
		 fb_pr($result);
			if($result['status']=="success"){
			redirect('/species');
		}
		
	}
public function list_record(){
		$table_name = "species";
		$this->fb_rest->test_list_record($table_name);
	}
	//
}
