<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testrest extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
	}

	public function index()
	{
		$email = "kamesh.veerachamy@fourbends.com"; // string | 
		$passwd = "welcome1234545"; // string | 
		$result = $this->fb_rest->login($email, $passwd);
		fb_pr($result);
	}
	
	public function login()
	{
		$email = "kamesh.veerachamy@fourbends.com"; // string | 
		$passwd = "welcome123"; // string | 
		$result = $this->fb_rest->login($email, $passwd);
		fb_pr($result);
	}
	
	public function testdata()
	{
		$token = $this->fb_rest->get_fbuser_data('user_token');
		fb_pr($token);
	}
	
	public function index1()
	{
		$aresult = null;
		require_once( APPPATH . 'third_party/boodskap/autoload.php');		
		$api_instance = new Swagger\Client\Api\LoginApi();
		$email = "saleem@fourbends.com"; // string | 
		$passwd = "welcome1235"; // string | 

		try {
			$aresult = $result = $api_instance->login($email, $passwd);
			
			$user_token = $result->getToken();
			$domain_key = $result->getDomainKey();
			$api_key = $result->getApiKey();
			$cUser = $result->getUser();
			$email = $cUser->getEmail();
			$first_name = $cUser->getFirstName();
			$last_name = $cUser->getLastName();
			$country = $cUser->getCountry();
			$state = $cUser->getState();
			$city = $cUser->getCity();
			$address = $cUser->getAddress();
			$zipcode = $cUser->getZipcode();
			$locale = $cUser->getLocale();
			$timezone = $cUser->getTimezone();
			
			$udata = array(
				"user_token" => $user_token,
				"domain_key" => $domain_key,
				"api_key" => $api_key,
			    "email" => $email,
				"first_name" => $first_name,
				"last_name" => $last_name,
				"country" => $country,
				"state" => $state,
				"city" => $city,
				"address" => $address,
				"zipcode" => $zipcode,
				"locale" => $locale,
				"timezone" => $timezone
			);
			
			$data["udata"] = $udata;
			$data['aresult'] = $aresult;
			$this->load->view('sample_result', $data);
		} catch (Exception $e) {
			$msg = $e->getMessage();
			log_message('error', "Exception when calling LoginApi->login: ".$msg);
		}
	}
	
	public function template(){
		$this->load->library('parser');
		$data = array(
        'blog_title' => 'My Blog Title',
        'blog_heading' => 'My Blog Heading',
		'blog_entries' => array(
							array('title' => "Test Title 1", 'body' => "Test body content 1"),
							array('title' => "Test Title 2", 'body' => "Test body content 2"),
							array('title' => "Test Title 3", 'body' => "Test body content 3"),
						  )
		);

		$this->parser->parse('templates/blog_template', $data);
	}
	
	public function time_ex(){
		//1519630427994
		//1519612195.499
		/* $date = new DateTime();
        echo $current_timestamp = $date->getTimestamp();
		echo "<br/>".time(); */
		// 1519632922885
		$this->load->helper('date');
		$t = 1519632922;
		echo  date("d m Y h:i:s a", $t);
		echo "<br/>";
		echo  now().' Now => '.date("d m Y h:i:s a", now());
		echo "<br/>";
		echo time().' Time => '.date("d m Y h:i:s a", time());
	}
	
	
	public function time_ex1(){
		$this->load->helper('date');
		$t = now();
		echo  date("d m Y h:i:s a",$t);
	}
	
	public function add_record(){
		$table_name = "ponds1";
		/* $idata = array("pondname" => "TK-p2", "depth" => "10.0", "height" => "5.5",
		"width" => "110.5", "built_date" => "23/05/2013",
		"createdtime" => now(), "updatedtime" => now());
		
		 exit; */
		
		$this->load->helper('date');
		
		$idatas =  array(
		
		  array("pondname" => "TK-p11", "depth" => "10.0", "height" => "5.5",
		"width" => "110.5", "built_date" => "11/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		  array("pondname" => "TK-p12", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "12/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p13", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "13/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p14", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "14/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p15", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "15/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p16", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "16/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p17", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "17/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p18", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "18/06/2013",
		"createdtime" => now(), "updatedtime" => now()) ,
		
		array("pondname" => "TK-p19", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "18/06/2013",
		"createdtime" => now(), "updatedtime" => now()) ,
		
		array("pondname" => "TK-p20", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "18/06/2013",
		"createdtime" => now(), "updatedtime" => now()) ,
		
		array("pondname" => "TK-p21", "depth" => "10.0", "height" => "5.5",
		"width" => "110.5", "built_date" => "11/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		  array("pondname" => "TK-p22", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "12/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p23", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "13/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p24", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "14/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p25", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "15/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p26", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "16/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p27", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "17/06/2013",
		"createdtime" => now(), "updatedtime" => now()),
		
		array("pondname" => "TK-p28", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "18/06/2013",
		"createdtime" => now(), "updatedtime" => now()) ,
		
		array("pondname" => "TK-p29", "depth" => "10.0", "height" => "6.0",
		"width" => "110.5", "built_date" => "18/06/2013",
		"createdtime" => now(), "updatedtime" => now()) ,
		
		array("pondname" => "TK-p30", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "18/06/2013",
		"createdtime" => now(), "updatedtime" => now())
		
		
		);
		
		foreach($idatas as $idata){
			$result = $this->fb_rest->create_record($table_name, $idata);
			fb_pr($result);
		}
		
		//$result = $this->fb_rest->create_record($table_name, $idata);
		//fb_pr($result);
		
	}
	
	public function create_record(){
		$table_name = "ponds1";
		$idata = array("pondname" => "TK-p32", "depth" => "10.0", "height" => "5.5",
		"width" => "120.0", "built_date" => "18/06/2013",
		"createdtime" => now(), "updatedtime" => now());
		$result = $this->fb_rest->create_record($table_name, $idata);
		fb_pr($result);
	}
	
	public function list_record(){
		$this->output->enable_profiler(TRUE);
		$this->fb_rest->test_list_record();
	}
	
	public function test_combo(){
		$table_name = "ponds1";
		$this->output->enable_profiler(TRUE);
		$msg = $this->fb_rest->combo_list($table_name);
		fb_pr($msg);
		if($msg["status"] == "success"){
			$cmb_list = $msg["combo_list"];
			echo "<select>";
			foreach($cmb_list as $ck => $cv){
				echo "<option value='$ck'>$cv</option>";
			}
			echo "</select>";
		}
	}
	
	public function test_combo1(){
		$table_name = "ponds1";
		$cmb_list = fb_combo_arr($table_name);
		echo "<select>";
		foreach($cmb_list as $ck => $cv){
			echo "<option value='$ck'>$cv</option>";
		}
		echo "</select>";
	}
	
	
	
}
