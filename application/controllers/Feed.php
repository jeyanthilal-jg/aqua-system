<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends CI_Controller {
	
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		
	}
	
	public function index()
	{
		if($this->fb_rest->isloggedin()){
  		    /* $this->load->view('include/header');
			$this->load->view('include/left_menu');
			$this->load->view('layout/feedlist_content');
			$this->load->view('include/footer'); */
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("per_page", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/feed");
			$this->table="feeds";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			$msg  = $this->fb_rest->list_record($params);
		
		
  		    $this->load->view('include/header');
			$this->load->view('include/left_menu');
			
			//fb_pr($msg);
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("layout/feedlist_content", $data);
			}else{
				//$this->load->view("error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
	}
	
	public function add_record(){
		$form_data = $this->input->post();
		
		$feedname = $this->input->post("feedname");
		$feed_type = $this->input->post("feed_type");
		$size = $this->input->post("size");
		
		$str=$feedname;
		//echo $str;
		if($str!=0 || $str<=25)
		{
			$table_name = "feeds";
			$idata = array("feedname" => $feedname, 
			"size" => $size, 
			"feed_type" => $feed_type,
			"createdtime" => now(), 
			"updatedtime" => now());
			
			$result = $this->fb_rest->create_record($table_name, $idata);
			 fb_pr($result);
				if($result['status']=="success"){
				redirect('/feed');
			}
		}/* else{
			echo "<script type='text/javascript'>alert('Pleas Enter Valid Feed Name')</script>";
			asdf
			redirect('/feed');
		} */
	}
	
	public function list_record(){
		$table_name = "feeds";
		$this->fb_rest->test_list_record($table_name);
	}
}
	?>